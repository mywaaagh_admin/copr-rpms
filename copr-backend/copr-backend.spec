%global prunerepo_version 1.20
%global tests_version 2
%global tests_tar test-data-copr-backend

%global copr_common_version 0.19

Name:       copr-backend
Version:    1.168
Release:    1%{?dist}
Summary:    Backend for Copr

License:    GPL-2.0-or-later
URL:        https://github.com/fedora-copr/copr

# Source is created by:
# git clone %%url && cd copr
# tito build --tgz --tag %%name-%%version-%%release
Source0:    %{name}-%{version}.tar.gz
Source1:    %{tests_tar}-%{tests_version}.tar.gz
Patch1:     helpers.patch
Patch2:     euler_msgbus.patch
Patch3:     print_queues.patch
#Patch4:     redis_helpers.patch # should patch to copr common
Patch5:     support_signatrust_backend.patch
Patch6:     signatrust_bin.patch

BuildArch:  noarch
BuildRequires: asciidoc
BuildRequires: createrepo_c >= 0.16.1
BuildRequires: libappstream-glib-builder
BuildRequires: libxslt
BuildRequires: make
BuildRequires: redis
BuildRequires: rsync
BuildRequires: systemd
BuildRequires: util-linux

BuildRequires: python3-devel
BuildRequires: python3-setuptools

BuildRequires: python3-copr
BuildRequires: python3-copr-common = %copr_common_version
BuildRequires: python3-daemon
BuildRequires: python3-dateutil
BuildRequires: python3-distro
BuildRequires: python3-filelock
BuildRequires: python3-gobject
BuildRequires: python3-httpretty
BuildRequires: python3-humanize
BuildRequires: python3-munch
BuildRequires: python3-netaddr
BuildRequires: python3-packaging
BuildRequires: python3-pytest
BuildRequires: python3-pytz
BuildRequires: python3-requests
BuildRequires: python3-resalloc
BuildRequires: python3-retask
BuildRequires: python3-setproctitle
BuildRequires: python3-sphinx
BuildRequires: python3-tabulate
BuildRequires: modulemd-tools >= 0.6
BuildRequires: prunerepo >= %prunerepo_version
BuildRequires: dnf
BuildRequires: rpmdevtools

Requires:   (copr-selinux if selinux-policy-targeted)
Requires:   ansible
Suggests:   awscli
Requires:   createrepo_c >= 0.16.1
Requires:   crontabs
Requires:   gawk
Requires:   libappstream-glib-builder
Requires:   lighttpd
Recommends: logrotate
Requires:   mock
Requires:   obs-signd
Requires:   openssh-clients
Requires:   prunerepo >= %prunerepo_version
Requires:   python3-copr
Requires:   python3-copr-common = %copr_common_version
Recommends: python3-copr-messaging
Requires:   python3-daemon
Requires:   python3-dateutil
Recommends: python3-fedmsg
Requires:   python3-filelock
Requires:   python3-gobject
Requires:   python3-humanize
Requires:   python3-jinja2
Requires:   python3-munch
Requires:   python3-netaddr
Requires:   python3-novaclient
Requires:   python3-packaging
Requires:   python3-pytz
Requires:   python3-requests
Requires:   python3-resalloc >= 3.0
Requires:   python3-retask
Requires:   python3-setproctitle
Requires:   python3-tabulate
Requires:   python3-boto3
Requires:   redis
Requires:   rpm-sign
Requires:   rsync
Requires:   modulemd-tools >= 0.6
Recommends: util-linux-core
Requires:   zstd

Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
COPR is lightweight build system. It allows you to create new project in WebUI,
and submit new builds and COPR will create yum repository from latest builds.

This package contains backend.

%package doc
Summary:    Code documentation for COPR backend

%description doc
COPR is lightweight build system. It allows you to create new project in WebUI,
and submit new builds and COPR will create yum repository from latests builds.

This package include documentation for COPR code. Mostly useful for developers
only.


%prep
%autosetup -a1 -p1



%build
make -C docs %{?_smp_mflags} html
%py3_build


%install
%py3_install


install -d %{buildroot}%{_sharedstatedir}/copr/public_html/results
install -d %{buildroot}%{_pkgdocdir}/lighttpd/
install -d %{buildroot}%{_sysconfdir}/copr
install -d %{buildroot}%{_sysconfdir}/logrotate.d/
install -d %{buildroot}%{_unitdir}
install -d %{buildroot}/%{_var}/log/copr-backend
install -d %{buildroot}/%{_var}/run/copr-backend/
install -d %{buildroot}/%{_tmpfilesdir}
install -d %{buildroot}/%{_sbindir}
install -d %{buildroot}%{_sysconfdir}/cron.daily
install -d %{buildroot}%{_sysconfdir}/cron.weekly
install -d %{buildroot}%{_sysconfdir}/sudoers.d
install -d %{buildroot}%{_bindir}/

cp -a copr-backend-service %{buildroot}/%{_sbindir}/
cp -a run/* %{buildroot}%{_bindir}/
cp -a conf/copr-be.conf.example %{buildroot}%{_sysconfdir}/copr/copr-be.conf

install -p -m 755 conf/crontab/daily %{buildroot}%{_sysconfdir}/cron.daily/copr-backend
install -p -m 755 conf/crontab/weekly  %{buildroot}%{_sysconfdir}/cron.weekly/copr-backend

cp -a conf/lighttpd/* %{buildroot}%{_pkgdocdir}/lighttpd/
cp -a conf/logrotate/* %{buildroot}%{_sysconfdir}/logrotate.d/
cp -a conf/tmpfiles.d/* %{buildroot}/%{_tmpfilesdir}

# for ghost files
touch %{buildroot}%{_var}/log/copr-backend/copr.log
touch %{buildroot}%{_var}/log/copr-backend/prune_old.log

cp -a units/*.{target,service} %{buildroot}/%{_unitdir}/
install -m 0644 conf/copr.sudoers.d %{buildroot}%{_sysconfdir}/sudoers.d/copr


install -d %{buildroot}%{_sysconfdir}/logstash.d

install -d %{buildroot}%{_datadir}/logstash/patterns/
cp -a conf/logstash/lighttpd.pattern %{buildroot}%{_datadir}/logstash/patterns/lighttpd.pattern

cp -a conf/playbooks %{buildroot}%{_pkgdocdir}/

install -d %{buildroot}%{_pkgdocdir}/examples/%{_sysconfdir}/logstash.d
cp -a conf/logstash/copr_backend.conf %{buildroot}%{_pkgdocdir}/examples/%{_sysconfdir}/logstash.d/copr_backend.conf

cp -a docs/build/html %{buildroot}%{_pkgdocdir}/


%check
./run_tests.sh -vv --no-cov

%pre
getent group copr >/dev/null || groupadd -r copr
getent passwd copr >/dev/null || \
useradd -r -g copr -G lighttpd -s /bin/bash -c "COPR user" copr
/usr/bin/passwd -l copr >/dev/null

%post
%systemd_post copr-backend.target

%preun
%systemd_preun copr-backend.target

%postun
%systemd_postun_with_restart copr-backend-log.service
%systemd_postun_with_restart copr-backend-build.service
%systemd_postun_with_restart copr-backend-action.service

%files
%license LICENSE
%python3_sitelib/copr_backend
%python3_sitelib/copr_backend*egg-info

%dir %{_sharedstatedir}/copr
%dir %attr(0755, copr, copr) %{_sharedstatedir}/copr/public_html/
%dir %attr(0755, copr, copr) %{_sharedstatedir}/copr/public_html/results
%dir %attr(0755, copr, copr) %{_var}/run/copr-backend
%dir %attr(0755, copr, copr) %{_var}/log/copr-backend

%ghost %{_var}/log/copr-backend/*.log

%config(noreplace) %{_sysconfdir}/logrotate.d/copr-backend
%dir %{_pkgdocdir}
%doc %{_pkgdocdir}/lighttpd
%doc %{_pkgdocdir}/playbooks
%dir %{_sysconfdir}/copr
%config(noreplace) %attr(0640, root, copr) %{_sysconfdir}/copr/copr-be.conf
%{_unitdir}/*.service
%{_unitdir}/*.target
%{_tmpfilesdir}/copr-backend.conf
%{_bindir}/*
%{_sbindir}/*

%config(noreplace) %{_sysconfdir}/cron.daily/copr-backend
%config(noreplace) %{_sysconfdir}/cron.weekly/copr-backend
%{_datadir}/logstash/patterns/lighttpd.pattern


%config(noreplace) %attr(0600, root, root)  %{_sysconfdir}/sudoers.d/copr

%files doc
%license LICENSE
%doc
%{_pkgdocdir}/
%exclude %{_pkgdocdir}/lighttpd
%exclude %{_pkgdocdir}/playbooks

%changelog
* Wed Mar 12 2025 lichaoran <pkwarcraft@hotmail.com> 1.168-1
- Init package