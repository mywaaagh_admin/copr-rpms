%global _empty_manifest_terminate_build 0
Name:		python-retry2
Version:	0.9.5
Release:	1
Summary:	Easy to use retry decorator.
License:	Apache License 2.0
URL:		https://github.com/eSAMTrade/retry
Source0:	%{name}-%{version}.tar.gz
BuildArch:	noarch

Requires:	(python3-decorator>=3.4.2)

%description
Easy to use retry decorator.
[This is a fork of https://github.com/invl/retry which is not maintained anymore]

%package -n python3-retry2
Summary:	Easy to use retry decorator.
Provides:	python-retry2
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-wheel
BuildRequires:	python3-pip
%description -n python3-retry2
Easy to use retry decorator.
[This is a fork of https://github.com/invl/retry which is not maintained anymore]

%package help
Summary:	Development documents and examples for retry2
Provides:	python3-retry2-doc
%description help
Easy to use retry decorator.
[This is a fork of https://github.com/invl/retry which is not maintained anymore]

%prep
%autosetup -n retry2-0.9.5

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch filelist.lst
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-retry2 -f filelist.lst
%{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Sep 27 2023 Python_Bot <Python_Bot@openeuler.org> - 0.9.5-1
- Package Spec generated
