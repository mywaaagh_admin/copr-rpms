%global _empty_manifest_terminate_build 0
Name:		python-XStatic-Bootstrap-SCSS
Version:	3.4.1.0
Release:	1
Summary:	Bootstrap-SCSS 3.4.1 (XStatic packaging standard)
License:	MIT
URL:		https://github.com/twbs/bootstrap-sass
Source0:	https://files.pythonhosted.org/packages/ff/05/f4aaaf2a001ef0453ae7bd6c69d194ae7ed3d35e76323381f6a56908b546/XStatic-Bootstrap-SCSS-3.4.1.0.tar.gz
BuildArch:	noarch


%description
Bootstrap style library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-Bootstrap-SCSS
Summary:	Bootstrap-SCSS 3.4.1 (XStatic packaging standard)
Provides:	python-XStatic-Bootstrap-SCSS
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  web-assets-devel
Requires:       web-assets-filesystem
%description -n python3-XStatic-Bootstrap-SCSS
Bootstrap style library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:	Development documents and examples for XStatic-Bootstrap-SCSS
Provides:	python3-XStatic-Bootstrap-SCSS-doc
%description help
Bootstrap style library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-Bootstrap-SCSS-3.4.1.0

%build
%py3_build

%install
%py3_install

mkdir -p %{buildroot}%{_jsdir}/bootstrap_scss
mv %{buildroot}%{python3_sitelib}/xstatic/pkg/bootstrap_scss/data/* %{buildroot}%{_jsdir}/bootstrap_scss
rmdir %{buildroot}%{python3_sitelib}/xstatic/pkg/bootstrap_scss/data/
# fix execute flags for js
chmod 644 %{buildroot}%{_jsdir}/bootstrap_scss/js/*.js
chmod 644 %{buildroot}%{_jsdir}/bootstrap_scss/js/bootstrap/*.js
 
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-Bootstrap-SCSS -f filelist.lst
%dir %{python3_sitelib}/*
%{_jsdir}/bootstrap_scss

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Jan 29 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
