%global _empty_manifest_terminate_build 0
Name:		python-XStatic-Patternfly
Version:	3.21.0.1
Release:	1
Summary:	Patternfly 3.21.0 (XStatic packaging standard)
License:	Apache v2
URL:		https://www.patternfly.org/
Source0:	https://files.pythonhosted.org/packages/4c/1e/3f69523bf8c2ff8d28d62d87237389ecea32471ab70408d34d14c4c4cb3d/XStatic-Patternfly-3.21.0.1.tar.gz
BuildArch:	noarch


%description
Patternfly style library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-Patternfly
Summary:	Patternfly 3.21.0 (XStatic packaging standard)
Provides:	python-XStatic-Patternfly
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
BuildRequires:  web-assets-devel
Requires:       web-assets-filesystem
%description -n python3-XStatic-Patternfly
Patternfly style library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:	Development documents and examples for XStatic-Patternfly
Provides:	python3-XStatic-Patternfly-doc
%description help
Patternfly style library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-Patternfly-3.21.0.1

%build
%py3_build

%install
%py3_install

mkdir -p %{buildroot}%{_jsdir}/patternfly
mv %{buildroot}%{python3_sitelib}/xstatic/pkg/patternfly/data/* %{buildroot}%{_jsdir}/patternfly
rmdir %{buildroot}%{python3_sitelib}/xstatic/pkg/patternfly/data/
# fix execute flags for js
chmod 644 %{buildroot}%{_jsdir}/patternfly/js/*.js

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-Patternfly -f filelist.lst
%dir %{python3_sitelib}/*
%{_jsdir}/patternfly

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu May 25 2023 Python_Bot <Python_Bot@openeuler.org> - 3.21.0.1-1
- Package Spec generated
