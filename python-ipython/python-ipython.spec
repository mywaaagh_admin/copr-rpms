%global _empty_manifest_terminate_build 0
Name:		python-ipython
Version:	8.14.0
Release:	1
Summary:	IPython: Productive Interactive Computing
License:	BSD-3-Clause
URL:		https://ipython.org
Source0:	https://files.pythonhosted.org/packages/fa/cb/2b777f625cca49b4a747b0dfe9986c21f5b46e5b548176903a914cdbec55/ipython-8.14.0.tar.gz
BuildArch:	noarch

Requires:	python3-backcall
Requires:	python3-decorator
Requires:	python3-jedi
Requires:	python3-matplotlib-inline
Requires:	python3-pickleshare
Requires:	python3-prompt-toolkit
Requires:	python3-pygments
Requires:	python3-stack-data
Requires:	python3-traitlets
Requires:	python3-typing-extensions
Requires:	python3-pexpect
Requires:	python3-appnope
Requires:	python3-colorama
Requires:	python3-black
Requires:	python3-ipykernel
Requires:	python3-setuptools
Requires:	python3-sphinx
Requires:	python3-sphinx-rtd-theme
Requires:	python3-docrepr
Requires:	python3-matplotlib
Requires:	python3-stack-data
Requires:	python3-pytest
Requires:	python3-typing-extensions
Requires:	python3-pytest
Requires:	python3-pytest-asyncio
Requires:	python3-testpath
Requires:	python3-nbconvert
Requires:	python3-nbformat
Requires:	python3-ipywidgets
Requires:	python3-notebook
Requires:	python3-ipyparallel
Requires:	python3-qtconsole
Requires:	python3-curio
Requires:	python3-matplotlib
Requires:	python3-numpy
Requires:	python3-pandas
Requires:	python3-trio
Requires:	python3-black
Requires:	python3-ipykernel
Requires:	python3-setuptools
Requires:	python3-sphinx
Requires:	python3-sphinx-rtd-theme
Requires:	python3-docrepr
Requires:	python3-matplotlib
Requires:	python3-stack-data
Requires:	python3-pytest
Requires:	python3-typing-extensions
Requires:	python3-pytest
Requires:	python3-pytest-asyncio
Requires:	python3-testpath
Requires:	python3-ipykernel
Requires:	python3-nbconvert
Requires:	python3-nbformat
Requires:	python3-ipywidgets
Requires:	python3-notebook
Requires:	python3-ipyparallel
Requires:	python3-qtconsole
Requires:	python3-pytest
Requires:	python3-pytest-asyncio
Requires:	python3-testpath
Requires:	python3-pytest
Requires:	python3-pytest-asyncio
Requires:	python3-testpath
Requires:	python3-curio
Requires:	python3-matplotlib
Requires:	python3-nbformat
Requires:	python3-numpy
Requires:	python3-pandas
Requires:	python3-trio

%description
IPython provides a rich toolkit to help you make the most out of using Python
interactively.  Its main components are:

 * A powerful interactive Python shell
 * A `Jupyter <https://jupyter.org/>`_ kernel to work with Python code in Jupyter
   notebooks and other interactive frontends.

The enhanced interactive Python shells have the following main features:

 * Comprehensive object introspection.

 * Input history, persistent across sessions.

 * Caching of output results during a session with automatically generated
   references.

 * Extensible tab completion, with support by default for completion of python
   variables and keywords, filenames and function keywords.

 * Extensible system of 'magic' commands for controlling the environment and
   performing many tasks related either to IPython or the operating system.

 * A rich configuration system with easy switching between different setups
   (simpler than changing $PYTHONSTARTUP environment variables every time).

 * Session logging and reloading.

 * Extensible syntax processing for special purpose situations.

 * Access to the system shell with user-extensible alias system.

 * Easily embeddable in other Python programs and GUIs.

 * Integrated access to the pdb debugger and the Python profiler.

The latest development version is always available from IPython's `GitHub
site <http://github.com/ipython>`_.


%package -n python3-ipython
Summary:	IPython: Productive Interactive Computing
Provides:	python-ipython
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-ipython
IPython provides a rich toolkit to help you make the most out of using Python
interactively.  Its main components are:

 * A powerful interactive Python shell
 * A `Jupyter <https://jupyter.org/>`_ kernel to work with Python code in Jupyter
   notebooks and other interactive frontends.

The enhanced interactive Python shells have the following main features:

 * Comprehensive object introspection.

 * Input history, persistent across sessions.

 * Caching of output results during a session with automatically generated
   references.

 * Extensible tab completion, with support by default for completion of python
   variables and keywords, filenames and function keywords.

 * Extensible system of 'magic' commands for controlling the environment and
   performing many tasks related either to IPython or the operating system.

 * A rich configuration system with easy switching between different setups
   (simpler than changing $PYTHONSTARTUP environment variables every time).

 * Session logging and reloading.

 * Extensible syntax processing for special purpose situations.

 * Access to the system shell with user-extensible alias system.

 * Easily embeddable in other Python programs and GUIs.

 * Integrated access to the pdb debugger and the Python profiler.

The latest development version is always available from IPython's `GitHub
site <http://github.com/ipython>`_.


%package help
Summary:	Development documents and examples for ipython
Provides:	python3-ipython-doc
%description help
IPython provides a rich toolkit to help you make the most out of using Python
interactively.  Its main components are:

 * A powerful interactive Python shell
 * A `Jupyter <https://jupyter.org/>`_ kernel to work with Python code in Jupyter
   notebooks and other interactive frontends.

The enhanced interactive Python shells have the following main features:

 * Comprehensive object introspection.

 * Input history, persistent across sessions.

 * Caching of output results during a session with automatically generated
   references.

 * Extensible tab completion, with support by default for completion of python
   variables and keywords, filenames and function keywords.

 * Extensible system of 'magic' commands for controlling the environment and
   performing many tasks related either to IPython or the operating system.

 * A rich configuration system with easy switching between different setups
   (simpler than changing $PYTHONSTARTUP environment variables every time).

 * Session logging and reloading.

 * Extensible syntax processing for special purpose situations.

 * Access to the system shell with user-extensible alias system.

 * Easily embeddable in other Python programs and GUIs.

 * Integrated access to the pdb debugger and the Python profiler.

The latest development version is always available from IPython's `GitHub
site <http://github.com/ipython>`_.


%prep
%autosetup -n ipython-8.14.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-ipython -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sun Jun 25 2023 Python_Bot <Python_Bot@openeuler.org> - 8.14.0-1
- Package Spec generated
