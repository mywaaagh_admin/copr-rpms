%global _empty_manifest_terminate_build 0
Name:		mysql-connector-python
Version:	8.0.32
Release:	1
Summary:	MySQL driver written in Python
License:	GNU GPLv2 (with FOSS License Exception)
URL:		http://dev.mysql.com/doc/connector-python/en/index.html
Source0:	https://dev.mysql.com/get/Downloads/Connector-Python/%{name}-%{version}-src.tar.gz

Requires:	python3-protobuf
Requires:	python3-lz4
Requires:	python3-zstandard
Requires:	python3-dnspython
Requires:	python3-gssapi

%description

MySQL driver written in Python which does not depend on MySQL C client
libraries and implements the DB API v2.0 specification (PEP-249).




%package -n mysql-connector-python3
Summary:	MySQL driver written in Python
Provides:	python-mysql-connector-python
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
BuildRequires:	python3-cffi
BuildRequires:	gcc
BuildRequires:	gdb
%description -n mysql-connector-python3

MySQL driver written in Python which does not depend on MySQL C client
libraries and implements the DB API v2.0 specification (PEP-249).




%package help
Summary:	Development documents and examples for mysql-connector-python
Provides:	python3-mysql-connector-python-doc
%description help

MySQL driver written in Python which does not depend on MySQL C client
libraries and implements the DB API v2.0 specification (PEP-249).




%prep
%autosetup -n %{name}-%{version}-src

%build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n mysql-connector-python3 -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Mar 27 2023 Python_Bot <Python_Bot@openeuler.org> - 8.0.32-1
- Package Spec generated
