%global _empty_manifest_terminate_build 0
Name:		python-templated-dictionary
Version:	1.2
Release:	1
Summary:	Dictionary with Jinja2 expansion
License:	GPL-2.0-or-later
URL:		https://github.com/xsuchy/templated-dictionary
Source0:	https://files.pythonhosted.org/packages/16/32/5e9726056345a1943d250ac51d2c17476f965495554d058c9dd308d790fa/templated-dictionary-1.2.tar.gz
BuildArch:	noarch

Requires:	python3-jinja2

%description
Dictionary where __getitem__() is run through Jinja2 template.


%package -n python3-templated-dictionary
Summary:	Dictionary with Jinja2 expansion
Provides:	python-templated-dictionary
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-templated-dictionary
Dictionary where __getitem__() is run through Jinja2 template.


%package help
Summary:	Development documents and examples for templated-dictionary
Provides:	python3-templated-dictionary-doc
%description help
Dictionary where __getitem__() is run through Jinja2 template.


%prep
%autosetup -n templated-dictionary-1.2

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-templated-dictionary -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sun Jun 25 2023 Python_Bot <Python_Bot@openeuler.org> - 1.2-1
- Package Spec generated
