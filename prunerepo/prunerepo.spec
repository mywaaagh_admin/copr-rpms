Name:    prunerepo
Version: 1.25
Summary: Remove old packages from rpm-md repository
Release: 1
Url: https://pagure.io/prunerepo

Source0: %name-%version.tar.gz

License: GPLv2+
BuildArch: noarch
BuildRequires: bash
BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: python3-rpm
BuildRequires: createrepo_c
BuildRequires: asciidoc
BuildRequires: findutils
BuildRequires: dnf
BuildRequires: dnf-plugins-core
BuildRequires: coreutils
Requires: createrepo_c
Requires: dnf-plugins-core
Requires: python3-rpm
Requires: python3

%description
RPM packages that have newer version available in that same
repository are deleted from filesystem and the rpm-md metadata are
recreated afterwards. If there is a source rpm for a deleted rpm
(and they both share the same directory path), then the source rpm
will be deleted as well.

Support for specific repository structure (e.g. COPR) is also available
making it possible to additionally remove build logs and whole build
directories associated with a package.

After deletion of obsoleted packages, the command
"createrepo_c --database --update" is called
to recreate the repository metadata.

%prep
%setup -q

%check
tests/run.sh

%build
name="%{name}" version="%{version}" summary="%{summary}" %py3_build
a2x -d manpage -f manpage man/prunerepo.1.asciidoc

%install
name="%{name}" version="%{version}" summary="%{summary}" %py3_install

install -d %{buildroot}%{_mandir}/man1
install -p -m 644 man/prunerepo.1 %{buildroot}/%{_mandir}/man1/

%files
%license LICENSE

%{python3_sitelib}/*
%{_bindir}/prunerepo
%{_mandir}/man1/prunerepo.1*

%changelog
* Tue Feb 18 2025 mywaaagh_admin <pkwarcraft@hotmail.com> 1.25-1
- Upgrade to 1.25
* Sun Jun 25 2023 lichaoran <pkwarcraft@hotmail.com> 1.21-1
- Initial package version
