Name:           nosync
Version:        1.1
Release:        1
Summary:        Preload library for disabling file's content synchronization
License:        ASL 2.0
URL:            http://github.com/kjn/%{name}
Source0:        http://github.com/kjn/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

# Eliminate dependency on ELF constructor ordering
# Solves segfaults during buildroot population in mock with nosync
# enabled for builds with openssl
# "FIPS module installed state definition is modified" changes
# https://bugzilla.redhat.com/show_bug.cgi?id=1837809
# https://github.com/kjn/nosync/pull/4
Patch0:         4.patch

BuildRequires:  make
BuildRequires:  gcc

%description
nosync is a small preload library that can be used to disable
synchronization of file's content with storage devices on GNU/Linux.
It works by overriding implementations of certain standard functions
like fsync or open.

%prep
%autosetup -p1

%build
make CFLAGS="%{optflags}" %{?_smp_mflags}

%install
%makeinstall

%files
%doc AUTHORS README.md
%{!?_licensedir:%global license %%doc}
%license LICENSE NOTICE
%{_libdir}/%{name}

%changelog
* Fri Mar 24 2023 lichaoran <pkwarcraft@hotmail.com> - 1.1-1
- Initial packaging
