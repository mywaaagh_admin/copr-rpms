%global _empty_manifest_terminate_build 0
Name:		python-Authlib
Version:	1.2.0
Release:	1
Summary:	The ultimate Python library in building OAuth and OpenID Connect servers and clients.
License:	BSD 3-Clause License
URL:		https://authlib.org/
Source0:	https://files.pythonhosted.org/packages/1e/84/3c82d181a04053fefa456dcb15edea93ffdb06071570b6cb05783f5e5fa5/Authlib-1.2.0.tar.gz
BuildArch:	noarch

Requires:	python3-cryptography

%description
The ultimate Python library in building OAuth and OpenID Connect servers.
JWS, JWK, JWA, JWT are included.

%package -n python3-Authlib
Summary:	The ultimate Python library in building OAuth and OpenID Connect servers and clients.
Provides:	python-Authlib
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-Authlib
The ultimate Python library in building OAuth and OpenID Connect servers.
JWS, JWK, JWA, JWT are included.

%package help
Summary:	Development documents and examples for Authlib
Provides:	python3-Authlib-doc
%description help
The ultimate Python library in building OAuth and OpenID Connect servers.
JWS, JWK, JWA, JWT are included.

%prep
%autosetup -n Authlib-1.2.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-Authlib -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jun 07 2023 lichaoran <pkwarcraft@hotmail.com> - 1.2.0-1
- Package Spec generated
