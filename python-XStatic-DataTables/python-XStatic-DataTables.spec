%global _empty_manifest_terminate_build 0
Name:		python-XStatic-DataTables
Version:	1.10.15.1
Release:	1
Summary:	DataTables 1.10.15 (XStatic packaging standard)
License:	(same as DataTables)
URL:		http://www.datatables.net
Source0:	https://files.pythonhosted.org/packages/3c/bb/d6a58cd1c4082bb5824db4eacb9293a882037793f41bef7bcaabc34ff9dd/XStatic-DataTables-1.10.15.1.tar.gz
BuildArch:	noarch


%description
The DataTables plugin for jQuery packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-DataTables
Summary:	DataTables 1.10.15 (XStatic packaging standard)
Provides:	python-XStatic-DataTables
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
BuildRequires:  web-assets-devel
Requires:       web-assets-filesystem
%description -n python3-XStatic-DataTables
The DataTables plugin for jQuery packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:	Development documents and examples for XStatic-DataTables
Provides:	python3-XStatic-DataTables-doc
%description help
The DataTables plugin for jQuery packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-DataTables-1.10.15.1

%build
%py3_build

%install
%py3_install
mkdir -p %{buildroot}%{_jsdir}/datatables
mv %{buildroot}%{python3_sitelib}/xstatic/pkg/datatables/data/* %{buildroot}%{_jsdir}/datatables
rmdir %{buildroot}%{python3_sitelib}/xstatic/pkg/datatables/data/
# fix execute flags for js
chmod 644 %{buildroot}%{_jsdir}/datatables/js/*.js

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-DataTables -f filelist.lst
%dir %{python3_sitelib}/*
%{_jsdir}/datatables

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu May 18 2023 Python_Bot <Python_Bot@openeuler.org> - 1.10.15.1-1
- Package Spec generated
