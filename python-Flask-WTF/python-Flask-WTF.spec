%global _empty_manifest_terminate_build 0
Name:		python-Flask-WTF
Version:	1.1.1
Release:	1
Summary:	Form rendering, validation, and CSRF protection for Flask with WTForms.
License:	BSD-3-Clause
URL:		https://github.com/wtforms/flask-wtf/
Source0:	https://files.pythonhosted.org/packages/80/55/5114035eb8f6200fbe838a4b9828409ac831408c4591bf7875aed299d5f8/Flask-WTF-1.1.1.tar.gz
BuildArch:	noarch

Requires:	python3-Flask
Requires:	python3-WTForms
Requires:	python3-itsdangerous
Requires:	python3-email-validator

%description
Simple integration of Flask and WTForms, including CSRF, file upload,
and reCAPTCHA.

%package -n python3-Flask-WTF
Summary:	Form rendering, validation, and CSRF protection for Flask with WTForms.
Provides:	python-Flask-WTF
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-Flask-WTF
Simple integration of Flask and WTForms, including CSRF, file upload,
and reCAPTCHA.

%package help
Summary:	Development documents and examples for Flask-WTF
Provides:	python3-Flask-WTF-doc
%description help
Simple integration of Flask and WTForms, including CSRF, file upload,
and reCAPTCHA.

%prep
%autosetup -n Flask-WTF-1.1.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-Flask-WTF -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jun 07 2023 Python_Bot <Python_Bot@openeuler.org> - 1.1.1-1
- Package Spec generated
