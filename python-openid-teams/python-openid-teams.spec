%global _empty_manifest_terminate_build 0
Name:		python-openid-teams
Version:	1.1
Release:	2
Summary:	This is an implementation of the OpenID teams extension for python-openid
License:	BSD
URL:		http://www.github.com/puiterwijk/python-openid-teams/
Source0:	https://mirrors.nju.edu.cn/pypi/web/packages/ce/a2/ae37cd9fd9067b0a01f22ca6261d0b4fbdb0b2398e20f4f00302c8cf5dff/python-openid-teams-1.1.tar.gz
BuildArch:	noarch


%description
UNKNOWN

%package -n python3-openid-teams
Summary:	This is an implementation of the OpenID teams extension for python-openid
Provides:	python-openid-teams
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
Requires:	python3-openid
%description -n python3-openid-teams
UNKNOWN

%package help
Summary:	Development documents and examples for python-openid-teams
Provides:	python3-openid-teams-doc
%description help
UNKNOWN

%prep
%autosetup -n python-openid-teams-1.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-openid-teams -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Mar 27 2023 lichaoran <pkwarcraft@hotmail.com> - 1.1-2
- add requires: python3-openid

* Sat Mar 25 2023 Python_Bot <Python_Bot@openeuler.org> - 1.1-1
- Package Spec generated
