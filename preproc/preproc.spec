# vim: syntax=spec

%global python_pkg python3
%global python /usr/bin/python3

Name: preproc
Version: 0.5
Release: 1
Summary: Simple text preprocessor
License: GPLv2+
URL: https://pagure.io/rpkg-util.git

# Source is created by:
# git clone https://pagure.io/rpkg-util.git
# cd rpkg-util/preproc
# git checkout preproc-0.5-1
# ./rpkg spec --sources
Source0: rpkg-util-preproc-bcad4393.tar.gz

# Upstream indicates non-maintenance (aside from security) and 3.11 in F37 deprecates the
# pipes module (it's been deprecated since 2.7?).
Patch0: 0001-pipes-to-shlex.patch

BuildArch: noarch

BuildRequires: %{python_pkg}
Requires: %{python_pkg}

%description
Simple text preprocessor implementing a very basic templating language.
You can use bash code enclosed in triple braces in a text file and
then pipe content of that file to preproc. preproc will replace each of
the tags with stdout of the executed code and print the final renderred
result to its own stdout.

%prep
%setup -T -b 0 -q -n rpkg-util-preproc
%patch0 -p1

%check
sed -i '1 s|#.*|#!%{python}|' preproc
./test

%install
install -d %{buildroot}%{_bindir}
install -p -m 0755 preproc %{buildroot}%{_bindir}

sed -i '1 s|#.*|#!%{python}|' %{buildroot}%{_bindir}/preproc

install -d %{buildroot}%{_mandir}/man1
install -p -m 0644 man/preproc.1 %{buildroot}%{_mandir}/man1

%files
%{!?_licensedir:%global license %doc}
%license LICENSE
%{_bindir}/preproc
%{_mandir}/man1/preproc.1*

%changelog
* Mon May 22 2023 lichaoran <pkwarcraft@hotmail.com> - 0.5-1
- Init package

