# vim: syntax=spec

%global python          /usr/bin/python3
%global python_build    %py3_build
%global python_install  %py3_install
%global python_sitelib  %python3_sitelib

Name: rpkg-util
Version: 3.2
Release: 1
Summary: RPM packaging utility
License: GPLv2+
URL: https://pagure.io/rpkg-util.git

Source0: rpkg-util-5cbe3a59.tar.gz
Patch0:  skip_nvre_test.patch

BuildArch: noarch

%description
This package contains the rpkg utility. We are putting
the actual 'rpkg' package into a subpackage because there already
exists package https://src.fedoraproject.org/rpms/rpkg. That package,
however, does not actually produce rpkg rpm whereas rpkg-util does.

%package -n rpkg
Summary: RPM packaging utility
BuildArch: noarch

BuildRequires: python3
BuildRequires: python3-setuptools
BuildRequires: python3-devel
BuildRequires: python3-mock
BuildRequires: python3-pytest
BuildRequires: python3-munch
BuildRequires: python3-rpm-macros
BuildRequires: python3-cached_property
BuildRequires: python3-rpm
BuildRequires: python3-pycurl
Requires: python3-cached_property
Requires: python3-munch
Requires: python3-rpm
Requires: python3-pycurl
# https://bugzilla.redhat.com/show_bug.cgi?id=2035475
Requires: python3-setuptools

BuildRequires: preproc
BuildRequires: rpkg-macros
Requires: preproc
Requires: rpkg-macros
Requires: rpm-build
Requires: cpio

%description -n rpkg
This is an RPM packaging utility that can work with both DistGit
and standard Git repositories and handles packed directory content
as well as unpacked one.

%prep
%autosetup -p1 -n rpkg-util

%check
PYTHON=%{python} ./unittests

%build
version=%{version} %python_build
%{python} man/rpkg_man_page.py > rpkg.1

%install
%{python_install}

sed -i '1 s|#.*|#!%{python}|' %{buildroot}%{_bindir}/rpkg

install -d %{buildroot}%{_mandir}/man1
install -p -m 0644 rpkg.1 %{buildroot}%{_mandir}/man1

install -d %{buildroot}%{_sysconfdir}
install -d %{buildroot}%{_datarootdir}/bash-completion/completions

cp -a rpkg.conf %{buildroot}%{_sysconfdir}/
cp -a rpkg.bash %{buildroot}%{_datarootdir}/bash-completion/completions/

%files -n rpkg
%{!?_licensedir:%global license %doc}
%license LICENSE
%{python_sitelib}/*

%config(noreplace) %{_sysconfdir}/rpkg.conf
%{_datadir}/bash-completion/completions/rpkg.bash

%{_bindir}/rpkg
%{_mandir}/*/*

%changelog
* Mon May 22 2023 lichaoran <pkwarcraft@hotmail.com> - 3.2-1
- Init package
