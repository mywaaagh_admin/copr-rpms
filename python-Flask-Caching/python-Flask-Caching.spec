%global _empty_manifest_terminate_build 0
Name:		python-Flask-Caching
Version:	2.0.2
Release:	1
Summary:	Adds caching support to Flask applications.
License:	BSD
URL:		https://github.com/pallets-eco/flask-caching
Source0:	https://files.pythonhosted.org/packages/0c/7c/64cf01e0c2c89927b386c98a107b6e400dbc24f3ac60639e95032c367f81/Flask-Caching-2.0.2.tar.gz
BuildArch:	noarch

Requires:	python3-cachelib
Requires:	python3-Flask

%description
A fork of the `Flask-cache`_ extension which adds easy cache support to Flask.

%package -n python3-Flask-Caching
Summary:	Adds caching support to Flask applications.
Provides:	python-Flask-Caching
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-Flask-Caching
A fork of the `Flask-cache`_ extension which adds easy cache support to Flask.

%package help
Summary:	Development documents and examples for Flask-Caching
Provides:	python3-Flask-Caching-doc
%description help
A fork of the `Flask-cache`_ extension which adds easy cache support to Flask.

%prep
%autosetup -n Flask-Caching-2.0.2

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-Flask-Caching -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jun 07 2023 Python_Bot <Python_Bot@openeuler.org> - 2.0.2-1
- Package Spec generated
