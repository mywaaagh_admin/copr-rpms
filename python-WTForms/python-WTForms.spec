%global _empty_manifest_terminate_build 0
Name:		python-WTForms
Version:	3.0.1
Release:	1
Summary:	Form validation and rendering for Python web development.
License:	BSD-3-Clause
URL:		https://wtforms.readthedocs.io/
Source0:	https://files.pythonhosted.org/packages/9a/7d/d4aa68f5bfcb91dd61a7faf0e862512ae7b3d531c41f24c217910aec0559/WTForms-3.0.1.tar.gz
BuildArch:	noarch

Requires:	python3-MarkupSafe
Requires:	python3-email-validator

%description
WTForms is a flexible forms validation and rendering library for Python
web development. It can work with whatever web framework and template
engine you choose. It supports data validation, CSRF protection,
internationalization (I18N), and more. There are various community
libraries that provide closer integration with popular frameworks.

%package -n python3-WTForms
Summary:	Form validation and rendering for Python web development.
Provides:	python-WTForms
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-WTForms
WTForms is a flexible forms validation and rendering library for Python
web development. It can work with whatever web framework and template
engine you choose. It supports data validation, CSRF protection,
internationalization (I18N), and more. There are various community
libraries that provide closer integration with popular frameworks.

%package help
Summary:	Development documents and examples for WTForms
Provides:	python3-WTForms-doc
%description help
WTForms is a flexible forms validation and rendering library for Python
web development. It can work with whatever web framework and template
engine you choose. It supports data validation, CSRF protection,
internationalization (I18N), and more. There are various community
libraries that provide closer integration with popular frameworks.

%prep
%autosetup -n WTForms-3.0.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-WTForms -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jun 07 2023 Python_Bot <Python_Bot@openeuler.org> - 3.0.1-1
- Package Spec generated
