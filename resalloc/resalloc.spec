%global srcname resalloc

%global sysuser  resalloc
%global sysgroup %sysuser
%global _logdir  %_var/log/%{name}server
%global _homedir %_sharedstatedir/%{name}server

%global agent_user  resalloc-agent-spawner
%global agent_group %agent_user

%global create_user_group() \
getent group "%1" >/dev/null || groupadd -r "%1" \
getent passwd "%1" >/dev/null || \\\
useradd -r -g "%2" -G "%2" -s "%3" \\\
        -c "%1 service user" "%1" \\\
        -d "%4"


%global sum Resource allocator for expensive resources
%global desc \
The resalloc project aims to help with taking care of dynamically \
allocated resources, for example ephemeral virtual machines used for \
the purposes of CI/CD tasks.

Name:       %srcname
Summary:    %sum - client tooling
Version:    5.9
Release:    1
License:    GPL-2.0-or-later
URL:        https://github.com/praiskup/resalloc
BuildArch:  noarch

BuildRequires: make
BuildRequires: postgresql-server


BuildRequires: python3-alembic
BuildRequires: python3-argparse-manpage
BuildRequires: python3-devel
BuildRequires: python3-psycopg2
BuildRequires: python3-pytest
BuildRequires: python3-pytest-cov
BuildRequires: python3-setuptools
BuildRequires: python3-six
BuildRequires: python3-sqlalchemy
BuildRequires: python3-pyyaml
BuildRequires: systemd

Requires:   python3-%srcname = %version-%release

Source0: https://github.com/praiskup/%name/releases/download/v%version/%name-%version.tar.gz
Source1: resalloc.service
Source2: logrotate
Source3: merge-hook-logs
Source4: cron.hourly
Source5: resalloc-agent-spawner.service
# GPL-2.0-or-later too
Source6: https://raw.githubusercontent.com/praiskup/wait-for-ssh/main/wait-for-ssh

%description
%desc

The %name package provides the client-side tooling.


%package server
Summary:    %sum - server part

Requires: crontabs
Requires: logrotate
Requires: python3-%srcname = %version-%release
Requires: %srcname-helpers = %version-%release
Requires: python3-alembic
Requires: python3-six
Requires: python3-sqlalchemy
Requires: python3-pyyaml

Requires(pre): /usr/sbin/useradd
%description server
%desc

The %name-server package provides the resalloc server, and
some tooling for resalloc administrators.


%package helpers
Summary:    %sum - helper/library scripts

%description helpers
%desc

Helper and library-like scripts for external Resalloc plugins like resalloc-aws,
resalloc-openstack, etc.

%package webui
Summary:    %sum - webui part

Requires: python3-%srcname = %version-%release
Requires: %name-server
Requires: python3-flask
Recommends: %name-selinux

%description webui
%desc

The %name-webui package provides the resalloc webui,
it shows page with information about resalloc resources.

%package agent-spawner
Summary: %sum - daemon starting agent-like resources

Requires(pre): /usr/sbin/useradd
Requires: python3-copr-common >= 0.23
Requires: python3-daemon
Requires: python3-redis
Requires: python3-resalloc
Requires: python3-setproctitle

%description agent-spawner
%desc

Agent Spawner maintains sets resources (agents) of certain kind and in certain
number, according to given configuration.  Typical Resalloc resource is
completely dummy, fully controlled from the outside.  With agent-like resources
this is different — such resources are self-standing, they take care of
themselves, perhaps interacting/competing with each other.  The only thing that
agent-spawner needs to do is to control the ideal number of them.


%package -n python3-%srcname
Summary: %sum - Python 3 client library
%{?python_provide:%python_provide python3-%srcname}
%description -n python3-%srcname
%desc

The python3-%name package provides Python 3 client library for talking
to the resalloc server.


%package selinux
Summary: SELinux module for %{name}
Requires: %name-webui = %version-%release
# Requires(post): policycoreutils-python
BuildRequires: selinux-policy-devel
%{?selinux_requires}

%description selinux
%desc

%post selinux
semanage fcontext -a -t httpd_sys_script_exec_t \
    %_var/www/cgi-%{name} 2>/dev/null || :
restorecon -R %_var/www/cgi-%{name} || :


%prep
%autosetup -p1 -n %name-%version


%build
%py3_build
sed "1c#! $python" %SOURCE6 > %{name}-wait-for-ssh


%install
%py3_install
install -d -m 755 %buildroot%_datadir/%{name}webui
cp -r %{name}webui/templates %buildroot%_datadir/%{name}webui/
cp -r %{name}webui/static %buildroot%_datadir/%{name}webui/

install -d -m 755 %buildroot%_var/www/
install -p -m 755 %{name}webui/cgi-resalloc %buildroot%_var/www/cgi-%{name}

mkdir -p %buildroot%_unitdir
mkdir -p %buildroot%_logdir
install -p -m 644 %SOURCE1 %buildroot%_unitdir
install -p -m 644 %SOURCE5 %buildroot%_unitdir
install -d -m 700 %buildroot%_homedir
install -d -m 700 %buildroot%_sysconfdir/logrotate.d
install -p -m 644 %SOURCE2 %buildroot%_sysconfdir/logrotate.d/resalloc-server
install -p -m 644 man/resalloc-server.1 %buildroot%_mandir/man1
install -d -m 755 %buildroot/%_libexecdir
install -p -m 755 %SOURCE3 %buildroot/%_libexecdir/%name-merge-hook-logs
install -d %buildroot%_sysconfdir/cron.hourly
install -p -m 755 %SOURCE4 %buildroot%_sysconfdir/cron.hourly/resalloc
install -p -m 755 %name-wait-for-ssh %buildroot%_bindir/%name-wait-for-ssh

# for now EUR call mock by root user, just skip check
# need re-enable check when builder run as non-root user
#%check
make check TEST_PYTHONS="python3"


# Simplify "alembic upgrade head" actions.
ln -s "%{python3_sitelib}/%{name}server" %buildroot%_homedir/project


%pre server
%create_user_group %sysuser %sysgroup /bin/bash %_homedir

%post server
%systemd_post resalloc.service

%postun server
%systemd_postun_with_restart resalloc.service

%pre agent-spawner
%create_user_group %agent_user %agent_group /bin/false /

%post agent-spawner
%systemd_post resalloc-agent-spawner.service

%postun agent-spawner
%systemd_postun_with_restart resalloc-agent-spawner.service

%global doc_files NEWS README.md

%files
%doc %doc_files
%license COPYING
%{_bindir}/%{name}
%_mandir/man1/%{name}.1*


%files -n python3-%srcname
%doc %doc_files
%license COPYING
%{python3_sitelib}/%{name}
%{python3_sitelib}/%{name}-*.egg-info


%files server
%doc %doc_files
%license COPYING
%{python3_sitelib}/%{name}server
%{_bindir}/%{name}-server
%{_bindir}/%{name}-maint
%attr(0750, %sysuser, %sysgroup) %dir %{_sysconfdir}/%{name}server
%config(noreplace) %{_sysconfdir}/%{name}server/*
%_unitdir/resalloc.service
%attr(0700, %sysuser, %sysgroup) %dir %_logdir
%_mandir/man1/%{name}-maint.1*
%_mandir/man1/%{name}-server.1*
%attr(0700, %sysuser, %sysgroup) %_homedir
%config %_sysconfdir/logrotate.d/resalloc-server
%_libexecdir/resalloc-merge-hook-logs
%config %attr(0755, root, root) %{_sysconfdir}/cron.hourly/resalloc


%files helpers
%doc %doc_files
%license COPYING
%{_bindir}/%{name}-check-vm-ip
%{_bindir}/%{name}-wait-for-ssh

%files agent-spawner
%_bindir/resalloc-agent*
%{python3_sitelib}/%{name}_agent_spawner
%_unitdir/resalloc-agent-spawner.service
%config(noreplace) %_sysconfdir/resalloc-agent-spawner

%files webui
%doc %doc_files
%license COPYING
%{python3_sitelib}/%{name}webui/
%_datadir/%{name}webui/
%_var/www/cgi-%{name}

%files selinux


%changelog
* Wed Mar 12 2025 lichaoran <pkwarcraft@hotmail.comm> - 5.9-1
- upgrade to 5.9

* Tue Mar 21 2023 lichaoran <pkwarcraft@hotmail.comm> - 4.91
- init package


