#! /bin/sh

jsname=jquery-ui
version=1.13.2

[ -r ${jsname}-${version}.tar.gz ] && \
    echo ${jsname}-${version}.tar.gz already exists && exit 1
[ -r ${jsname}-${version}-node-modules.tar.gz ] && \
    echo ${jsname}-${version}-node-modules.tar.gz already exists && exit 1

wget -N https://github.com/jquery/${jsname}/archive/${version}/${jsname}-${version}.tar.gz

curdir=$(pwd)
tmpdir=$(mktemp -d)

cd ${tmpdir}

# We need to bundle build dependencies since they are no longer
# available in Fedora. This uses the same technique as the js-jquery
# package.

tar -z -x -f ${curdir}/${jsname}-${version}.tar.gz
cd ${jsname}-${version}

# Reduce the dev dependencies in the package.json file by removing
# stuff not needed for the build. This reduces the size of the bundled
# build dependencies considerably.

patch -p1 <<EOF
--- a/package.json
+++ b/package.json
@@ -48,27 +48,15 @@
 		"test": "grunt"
 	},
 	"dependencies": {
-		"jquery": ">=1.8.0 <4.0.0"
 	},
 	"devDependencies": {
-		"commitplease": "3.2.0",
-		"eslint-config-jquery": "3.0.0",
 		"glob": "7.2.0",
 		"grunt": "1.5.3",
-		"grunt-bowercopy": "1.2.5",
 		"grunt-cli": "1.4.3",
-		"grunt-compare-size": "0.4.2",
 		"grunt-contrib-concat": "1.0.1",
-		"grunt-contrib-csslint": "2.0.0",
-		"grunt-contrib-qunit": "5.1.1",
 		"grunt-contrib-requirejs": "1.0.0",
 		"grunt-contrib-uglify": "5.0.1",
-		"grunt-eslint": "23.0.0",
-		"grunt-git-authors": "3.2.0",
-		"grunt-html": "14.5.0",
-		"load-grunt-tasks": "5.1.0",
-		"rimraf": "3.0.2",
-		"testswarm": "1.1.2"
+		"load-grunt-tasks": "5.1.0"
 	},
 	"keywords": []
 }
EOF

npm install --save-dev

# This file is not needed for the build
# It just needs to exist to avoid an error
mkdir -p node_modules/grunt-contrib-qunit/chrome
touch node_modules/grunt-contrib-qunit/chrome/bridge.js

tar -z -c --group root --owner root -f ${curdir}/${jsname}-${version}-node-modules.tar.gz node_modules

cd ${curdir}
rm -rf ${tmpdir}
