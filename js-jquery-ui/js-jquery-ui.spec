%global jsname jquery-ui

Name:		js-%{jsname}
Version:	1.13.2
Release:	1
Summary:	jQuery user interface

License:	MIT
URL:		https://jqueryui.com/
Source0:	https://github.com/jquery/%{jsname}/archive/%{version}/%{jsname}-%{version}.tar.gz
#		We need to bundle build dependencies since they are no
#		longer available in Fedora. This uses the same
#		technique as the js-jquery package.
Source1:	%{jsname}-%{version}-node-modules.tar.gz
#		Script to create the above sources
Source2:	create-source.sh

BuildArch:	noarch
BuildRequires:	nodejs >= 1:10
BuildRequires:	web-assets-devel
BuildRequires:	python3-rcssmin
Requires:	js-jquery >= 1.8.0
Requires:	web-assets-filesystem

%description
A curated set of user interface interactions, effects, widgets, and
themes built on top of the jQuery JavaScript Library.

%prep
%setup -q -n %{jsname}-%{version} -a 1
rm -rf dist

%build
./node_modules/grunt-cli/bin/grunt -v requirejs:js concat:css uglify:main

# Provide a compressed version of the cascading style sheet
python3 -m rcssmin -b < dist/jquery-ui.css > dist/jquery-ui.min.css

%install
mkdir -p %{buildroot}%{_jsdir}/%{jsname}
install -m 644 -p dist/* %{buildroot}%{_jsdir}/%{jsname}
mkdir -p %{buildroot}%{_jsdir}/%{jsname}/images
install -m 644 -p themes/base/images/* %{buildroot}%{_jsdir}/%{jsname}/images

%files
%{_jsdir}/%{jsname}
%license LICENSE.txt
%doc AUTHORS.txt CONTRIBUTING.md README.md

%changelog
* Wed May 17 2023 lichaoran <pkwarcraft@hotmail.com> - 1.13.2-1
- Init package
