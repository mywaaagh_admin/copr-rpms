%global _empty_manifest_terminate_build 0
Name:		python-prompt-toolkit
Version:	3.0.38
Release:	1
Summary:	Library for building powerful interactive command lines in Python
License:	BSD License
URL:		https://github.com/prompt-toolkit/python-prompt-toolkit
Source0:	https://files.pythonhosted.org/packages/4b/bb/75cdcd356f57d17b295aba121494c2333d26bfff1a837e6199b8b83c415a/prompt_toolkit-3.0.38.tar.gz
BuildArch:	noarch

Requires:	python3-wcwidth

%description
|Build Status|  |AppVeyor|  |PyPI|  |RTD|  |License|  |Codecov|
``prompt_toolkit`` *is a library for building powerful interactive command line applications in Python.*
Read the `documentation on readthedocs
<http://python-prompt-toolkit.readthedocs.io/en/stable/>`_.
NOTICE: prompt_toolkit 3.0
**************************
Please notice that this branch is the ``prompt_toolkit`` **3.0** branch. For most
users, it should be compatible with ``prompt_toolkit`` **2.0**, but it requires at
least **Python 3.6**. On the plus side, ``prompt_toolkit`` **3.0** is completely type
annotated and uses asyncio natively.
Gallery
*******
`ptpython <http://github.com/prompt-toolkit/ptpython/>`_ is an interactive
Python Shell, build on top of ``prompt_toolkit``.
`More examples <https://python-prompt-toolkit.readthedocs.io/en/stable/pages/gallery.html>`_
prompt_toolkit features
***********************
``prompt_toolkit`` could be a replacement for `GNU readline
<https://tiswww.case.edu/php/chet/readline/rltop.html>`_, but it can be much
more than that.
Some features:
- **Pure Python**.
- Syntax highlighting of the input while typing. (For instance, with a Pygments lexer.)
- Multi-line input editing.
- Advanced code completion.
- Both Emacs and Vi key bindings. (Similar to readline.)
- Even some advanced Vi functionality, like named registers and digraphs.
- Reverse and forward incremental search.
- Works well with Unicode double width characters. (Chinese input.)
- Selecting text for copy/paste. (Both Emacs and Vi style.)
- Support for `bracketed paste <https://cirw.in/blog/bracketed-paste>`_.
- Mouse support for cursor positioning and scrolling.
- Auto suggestions. (Like `fish shell <http://fishshell.com/>`_.)
- Multiple input buffers.
- No global state.
- Lightweight, the only dependencies are Pygments and wcwidth.
- Runs on Linux, OS X, FreeBSD, OpenBSD and Windows systems.
- And much more...
Feel free to create tickets for bugs and feature requests, and create pull
requests if you have nice patches that you would like to share with others.
Installation
************
    pip install prompt_toolkit
For Conda, do:
    conda install -c https://conda.anaconda.org/conda-forge prompt_toolkit
About Windows support
*********************
``prompt_toolkit`` is cross platform, and everything that you build on top
should run fine on both Unix and Windows systems. Windows support is best on
recent Windows 10 builds, for which the command line window supports vt100
escape sequences. (If not supported, we fall back to using Win32 APIs for color
and cursor movements).
It's worth noting that the implementation is a "best effort of what is
possible". Both Unix and Windows terminals have their limitations. But in
general, the Unix experience will still be a little better.
For Windows, it's recommended to use either `cmder
<http://cmder.net/>`_ or `conemu <https://conemu.github.io/>`_.
Getting started
***************
The most simple example of the library would look like this:
    from prompt_toolkit import prompt
    if __name__ == '__main__':
        answer = prompt('Give me some input: ')
        print('You said: %s' % answer)
For more complex examples, have a look in the ``examples`` directory. All
examples are chosen to demonstrate only one thing. Also, don't be afraid to
look at the source code. The implementation of the ``prompt`` function could be
a good start.
Philosophy
**********
The source code of ``prompt_toolkit`` should be **readable**, **concise** and
**efficient**. We prefer short functions focusing each on one task and for which
the input and output types are clearly specified. We mostly prefer composition
over inheritance, because inheritance can result in too much functionality in
the same object. We prefer immutable objects where possible (objects don't
change after initialization). Reusability is important. We absolutely refrain
from having a changing global state, it should be possible to have multiple
independent instances of the same code in the same process. The architecture
should be layered: the lower levels operate on primitive operations and data
structures giving -- when correctly combined -- all the possible flexibility;
while at the higher level, there should be a simpler API, ready-to-use and
sufficient for most use cases. Thinking about algorithms and efficiency is
important, but avoid premature optimization.
`Projects using prompt_toolkit <PROJECTS.rst>`_
***********************************************
Special thanks to
*****************
- `Pygments <http://pygments.org/>`_: Syntax highlighter.
- `wcwidth <https://github.com/jquast/wcwidth>`_: Determine columns needed for a wide characters.

%package -n python3-prompt-toolkit
Summary:	Library for building powerful interactive command lines in Python
Provides:	python-prompt-toolkit
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-prompt-toolkit
|Build Status|  |AppVeyor|  |PyPI|  |RTD|  |License|  |Codecov|
``prompt_toolkit`` *is a library for building powerful interactive command line applications in Python.*
Read the `documentation on readthedocs
<http://python-prompt-toolkit.readthedocs.io/en/stable/>`_.
NOTICE: prompt_toolkit 3.0
**************************
Please notice that this branch is the ``prompt_toolkit`` **3.0** branch. For most
users, it should be compatible with ``prompt_toolkit`` **2.0**, but it requires at
least **Python 3.6**. On the plus side, ``prompt_toolkit`` **3.0** is completely type
annotated and uses asyncio natively.
Gallery
*******
`ptpython <http://github.com/prompt-toolkit/ptpython/>`_ is an interactive
Python Shell, build on top of ``prompt_toolkit``.
`More examples <https://python-prompt-toolkit.readthedocs.io/en/stable/pages/gallery.html>`_
prompt_toolkit features
***********************
``prompt_toolkit`` could be a replacement for `GNU readline
<https://tiswww.case.edu/php/chet/readline/rltop.html>`_, but it can be much
more than that.
Some features:
- **Pure Python**.
- Syntax highlighting of the input while typing. (For instance, with a Pygments lexer.)
- Multi-line input editing.
- Advanced code completion.
- Both Emacs and Vi key bindings. (Similar to readline.)
- Even some advanced Vi functionality, like named registers and digraphs.
- Reverse and forward incremental search.
- Works well with Unicode double width characters. (Chinese input.)
- Selecting text for copy/paste. (Both Emacs and Vi style.)
- Support for `bracketed paste <https://cirw.in/blog/bracketed-paste>`_.
- Mouse support for cursor positioning and scrolling.
- Auto suggestions. (Like `fish shell <http://fishshell.com/>`_.)
- Multiple input buffers.
- No global state.
- Lightweight, the only dependencies are Pygments and wcwidth.
- Runs on Linux, OS X, FreeBSD, OpenBSD and Windows systems.
- And much more...
Feel free to create tickets for bugs and feature requests, and create pull
requests if you have nice patches that you would like to share with others.
Installation
************
    pip install prompt_toolkit
For Conda, do:
    conda install -c https://conda.anaconda.org/conda-forge prompt_toolkit
About Windows support
*********************
``prompt_toolkit`` is cross platform, and everything that you build on top
should run fine on both Unix and Windows systems. Windows support is best on
recent Windows 10 builds, for which the command line window supports vt100
escape sequences. (If not supported, we fall back to using Win32 APIs for color
and cursor movements).
It's worth noting that the implementation is a "best effort of what is
possible". Both Unix and Windows terminals have their limitations. But in
general, the Unix experience will still be a little better.
For Windows, it's recommended to use either `cmder
<http://cmder.net/>`_ or `conemu <https://conemu.github.io/>`_.
Getting started
***************
The most simple example of the library would look like this:
    from prompt_toolkit import prompt
    if __name__ == '__main__':
        answer = prompt('Give me some input: ')
        print('You said: %s' % answer)
For more complex examples, have a look in the ``examples`` directory. All
examples are chosen to demonstrate only one thing. Also, don't be afraid to
look at the source code. The implementation of the ``prompt`` function could be
a good start.
Philosophy
**********
The source code of ``prompt_toolkit`` should be **readable**, **concise** and
**efficient**. We prefer short functions focusing each on one task and for which
the input and output types are clearly specified. We mostly prefer composition
over inheritance, because inheritance can result in too much functionality in
the same object. We prefer immutable objects where possible (objects don't
change after initialization). Reusability is important. We absolutely refrain
from having a changing global state, it should be possible to have multiple
independent instances of the same code in the same process. The architecture
should be layered: the lower levels operate on primitive operations and data
structures giving -- when correctly combined -- all the possible flexibility;
while at the higher level, there should be a simpler API, ready-to-use and
sufficient for most use cases. Thinking about algorithms and efficiency is
important, but avoid premature optimization.
`Projects using prompt_toolkit <PROJECTS.rst>`_
***********************************************
Special thanks to
*****************
- `Pygments <http://pygments.org/>`_: Syntax highlighter.
- `wcwidth <https://github.com/jquast/wcwidth>`_: Determine columns needed for a wide characters.

%package help
Summary:	Development documents and examples for prompt-toolkit
Provides:	python3-prompt-toolkit-doc
%description help
|Build Status|  |AppVeyor|  |PyPI|  |RTD|  |License|  |Codecov|
``prompt_toolkit`` *is a library for building powerful interactive command line applications in Python.*
Read the `documentation on readthedocs
<http://python-prompt-toolkit.readthedocs.io/en/stable/>`_.
NOTICE: prompt_toolkit 3.0
**************************
Please notice that this branch is the ``prompt_toolkit`` **3.0** branch. For most
users, it should be compatible with ``prompt_toolkit`` **2.0**, but it requires at
least **Python 3.6**. On the plus side, ``prompt_toolkit`` **3.0** is completely type
annotated and uses asyncio natively.
Gallery
*******
`ptpython <http://github.com/prompt-toolkit/ptpython/>`_ is an interactive
Python Shell, build on top of ``prompt_toolkit``.
`More examples <https://python-prompt-toolkit.readthedocs.io/en/stable/pages/gallery.html>`_
prompt_toolkit features
***********************
``prompt_toolkit`` could be a replacement for `GNU readline
<https://tiswww.case.edu/php/chet/readline/rltop.html>`_, but it can be much
more than that.
Some features:
- **Pure Python**.
- Syntax highlighting of the input while typing. (For instance, with a Pygments lexer.)
- Multi-line input editing.
- Advanced code completion.
- Both Emacs and Vi key bindings. (Similar to readline.)
- Even some advanced Vi functionality, like named registers and digraphs.
- Reverse and forward incremental search.
- Works well with Unicode double width characters. (Chinese input.)
- Selecting text for copy/paste. (Both Emacs and Vi style.)
- Support for `bracketed paste <https://cirw.in/blog/bracketed-paste>`_.
- Mouse support for cursor positioning and scrolling.
- Auto suggestions. (Like `fish shell <http://fishshell.com/>`_.)
- Multiple input buffers.
- No global state.
- Lightweight, the only dependencies are Pygments and wcwidth.
- Runs on Linux, OS X, FreeBSD, OpenBSD and Windows systems.
- And much more...
Feel free to create tickets for bugs and feature requests, and create pull
requests if you have nice patches that you would like to share with others.
Installation
************
    pip install prompt_toolkit
For Conda, do:
    conda install -c https://conda.anaconda.org/conda-forge prompt_toolkit
About Windows support
*********************
``prompt_toolkit`` is cross platform, and everything that you build on top
should run fine on both Unix and Windows systems. Windows support is best on
recent Windows 10 builds, for which the command line window supports vt100
escape sequences. (If not supported, we fall back to using Win32 APIs for color
and cursor movements).
It's worth noting that the implementation is a "best effort of what is
possible". Both Unix and Windows terminals have their limitations. But in
general, the Unix experience will still be a little better.
For Windows, it's recommended to use either `cmder
<http://cmder.net/>`_ or `conemu <https://conemu.github.io/>`_.
Getting started
***************
The most simple example of the library would look like this:
    from prompt_toolkit import prompt
    if __name__ == '__main__':
        answer = prompt('Give me some input: ')
        print('You said: %s' % answer)
For more complex examples, have a look in the ``examples`` directory. All
examples are chosen to demonstrate only one thing. Also, don't be afraid to
look at the source code. The implementation of the ``prompt`` function could be
a good start.
Philosophy
**********
The source code of ``prompt_toolkit`` should be **readable**, **concise** and
**efficient**. We prefer short functions focusing each on one task and for which
the input and output types are clearly specified. We mostly prefer composition
over inheritance, because inheritance can result in too much functionality in
the same object. We prefer immutable objects where possible (objects don't
change after initialization). Reusability is important. We absolutely refrain
from having a changing global state, it should be possible to have multiple
independent instances of the same code in the same process. The architecture
should be layered: the lower levels operate on primitive operations and data
structures giving -- when correctly combined -- all the possible flexibility;
while at the higher level, there should be a simpler API, ready-to-use and
sufficient for most use cases. Thinking about algorithms and efficiency is
important, but avoid premature optimization.
`Projects using prompt_toolkit <PROJECTS.rst>`_
***********************************************
Special thanks to
*****************
- `Pygments <http://pygments.org/>`_: Syntax highlighter.
- `wcwidth <https://github.com/jquast/wcwidth>`_: Determine columns needed for a wide characters.

%prep
%autosetup -n prompt_toolkit-3.0.38

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-prompt-toolkit -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu May 18 2023 Python_Bot <Python_Bot@openeuler.org> - 3.0.38-1
- Package Spec generated
