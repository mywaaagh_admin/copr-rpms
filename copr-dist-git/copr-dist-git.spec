%global copr_common_version 0.19

Name:       copr-dist-git
Version:    0.66
Release:    1%{?dist}
Summary:    Copr services for Dist Git server

License:    GPL-2.0-or-later
URL:        https://github.com/fedora-copr/copr

# Source is created by:
# git clone %%url && cd copr
# tito build --tgz --tag %%name-%%version-%%release
Source0:    %name-%version.tar.gz

BuildArch:  noarch

BuildRequires: systemd
BuildRequires: python3-devel
BuildRequires: python3-munch
BuildRequires: python3-requests
BuildRequires: python3-rpkg
BuildRequires: python3-pytest
BuildRequires: python3-copr-common = %copr_common_version
BuildRequires: python3-oslo-concurrency
BuildRequires: python3-redis
BuildRequires: python3-setproctitle

Recommends: logrotate
Requires: systemd
Requires: httpd
Requires: coreutils
Requires: /usr/bin/crudini
Requires: dist-git
Requires: python3-copr-common = %copr_common_version
Requires: python3-requests
Requires: python3-rpkg >= 1.63-5
Requires: python3-munch
Requires: python3-oslo-concurrency
Requires: python3-setproctitle
Requires: python3-daemon
Requires: python3-redis
Requires: findutils
Requires: (copr-selinux if selinux-policy-targeted)
Requires: crontabs
Requires: redis

Recommends: python3-copr

%{?fedora:Requires(post): policycoreutils-python-utils}
%{?rhel:Requires(post): policycoreutils-python}

%description
COPR is lightweight build system. It allows you to create new project in WebUI
and submit new builds and COPR will create yum repository from latest builds.

This package contains Copr services for Dist Git server.


%prep
%setup -q


%build
%py3_build


%pre
getent group packager >/dev/null || groupadd -r packager
getent group copr-dist-git >/dev/null || groupadd -r copr-dist-git
getent group apache >/dev/null || groupadd -r apache
getent passwd copr-dist-git >/dev/null || \
useradd -r -m -g copr-dist-git -G packager,apache -c "copr-dist-git user" copr-dist-git
/usr/bin/passwd -l copr-dist-git >/dev/null

%install
%py3_install

install -d %{buildroot}%{_datadir}/copr/dist_git
install -d %{buildroot}%{_sysconfdir}/copr
install -d %{buildroot}%{_sysconfdir}/logrotate.d/
install -d %{buildroot}%{_sysconfdir}/httpd/conf.d/
install -d %{buildroot}%{_unitdir}
install -d %{buildroot}%{_var}/log/copr-dist-git
install -d %{buildroot}%{_tmpfilesdir}
install -d %{buildroot}%{_sharedstatedir}/copr-dist-git
install -d %{buildroot}%{_sysconfdir}/cron.monthly

install -p -m 755 conf/cron.monthly/copr-dist-git %{buildroot}%{_sysconfdir}/cron.monthly/copr-dist-git

cp -a conf/copr-dist-git.conf.example %{buildroot}%{_sysconfdir}/copr/copr-dist-git.conf
cp -a conf/httpd/copr-dist-git.conf %{buildroot}%{_sysconfdir}/httpd/conf.d/copr-dist-git.conf
cp -a conf/tmpfiles.d/* %{buildroot}/%{_tmpfilesdir}
cp -a copr-dist-git.service %{buildroot}%{_unitdir}/

cp -a conf/logrotate %{buildroot}%{_sysconfdir}/logrotate.d/copr-dist-git

mv %{buildroot}%{_bindir}/remove_unused_sources %{buildroot}%{_bindir}/copr-prune-dist-git-sources

# for ghost files
touch %{buildroot}%{_var}/log/copr-dist-git/main.log

%check
./run_tests.sh -vv --no-cov

%post
%systemd_post copr-dist-git.service

%preun
%systemd_preun copr-dist-git.service

%postun
%systemd_postun_with_restart copr-dist-git.service

%files
%license LICENSE
%python3_sitelib/copr_dist_git
%python3_sitelib/copr_dist_git*egg-info

%{_bindir}/*
%dir %{_datadir}/copr
%{_datadir}/copr/*
%dir %{_sysconfdir}/copr
%config(noreplace) %attr(0640, root, copr-dist-git) %{_sysconfdir}/copr/copr-dist-git.conf
%config(noreplace) %attr(0644, root, root) %{_sysconfdir}/httpd/conf.d/copr-dist-git.conf
%config(noreplace) %attr(0755, root, root) %{_sysconfdir}/cron.monthly/copr-dist-git

%dir %attr(0755, copr-dist-git, copr-dist-git) %{_sharedstatedir}/copr-dist-git/

%{_unitdir}/copr-dist-git.service

%dir %{_sysconfdir}/logrotate.d
%config(noreplace) %{_sysconfdir}/logrotate.d/copr-dist-git
%attr(0755, copr-dist-git, copr-dist-git) %{_var}/log/copr-dist-git
%attr(0644, copr-dist-git, copr-dist-git) %{_var}/log/copr-dist-git/main.log
%ghost %{_var}/log/copr-dist-git/*.log
%{_tmpfilesdir}/copr-dist-git.conf

%changelog
* Wed Feb 19 2025 mywaaagh_admin <pkwarcraft@hotmail.com> 0.66-1
- Init package
