Name:           crudini
Version:        0.9.4
Release:        1
Summary:        A utility for manipulating ini files

License:        GPLv2
URL:            https://github.com/pixelb/%{name}
Source0:        https://github.com/pixelb/%{name}/releases/download/%{version}/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  diffutils
BuildRequires:  grep
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-iniparse >= 0.3.2
Requires:       python3-iniparse >= 0.3.2

Patch0:         crudini-py3.patch

%description
A utility for easily handling ini files from the command line and shell
scripts.

%prep
%setup -q
%patch0 -p1

%build

%install
install -p -D -m 0755 %{name}.py %{buildroot}%{_bindir}/%{name}
install -p -D -m 0644 %{name}.1 %{buildroot}%{_mandir}/man1/%{name}.1

%check
pushd tests
LC_ALL=en_US.utf8 ./test.sh
popd

%files
%doc README.md COPYING TODO NEWS example.ini
%{_bindir}/%{name}
%{_mandir}/man1/*


%changelog
* Thu Mar 23 2023 lichaoran <pkwarcraft@hotmail.com> - 0.9.4-1
- Initial package
